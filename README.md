# Selenium in .NET: First script run

**Estimated reading time**: 15 min

## Story Outline
In this coding story, you will learn about a simple Selenium WebDriver script written in C#. The code snippet will demonstrate how to initiate a Selenium WebDriver session to control the Chrome browser, navigate to the EPAM website, locate web elements using various techniques, interact with the page using mouse clicks and keyboard input, and manage explicit wait conditions to ensure correct element identification.  

## Story Organization
**Story Branch**: master
> `git checkout master`


Tags: #Selenium, #.NET, #C_Sharp